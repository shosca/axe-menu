Axe Menu
========

Port of the Axe menu extension to gnome-shell 3.6

Control
--------
* The menu is bound to Super + a.
* Left click to open the menu >> start typing right away to search for what you're looking for >> press Enter to open the first search result.
* Press "Tab" on your keyboard to switch between Favorites and All applications.
* The bold and underlined texts are your favorites.
* To add an app to favorites, hover that app, right click. A box will appear, click Yes to confirm.
* To remove an app from favorite, hover that app (the bold and underlined ones), right click. A box will appear, click Yes to confirm.

Installation
------------

<pre>
git clone https://github.com/shosca/axe-menu.git ~/.local/share/gnome-shell/extensions/axemenu@wheezy
glib-compile-schemas ~/.local/share/gnome-shell/extensions/axemenu@wheezy/schemas/
</pre>
then restart shell.
